Integrates your website with embAd.

embAd is an awesome free service that
helps website owners to embed ads into their website,
giving them full control on ads placements and layout
using a drag & drop interface.
