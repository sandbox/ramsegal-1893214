<?php

/**
 * @file
 * Administrative page callbacks for the embad module.
 */

/**
 * Form builder; embAD admin settings form.
 */
function embad_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['embad_initials'] = array(
    '#value' => t('This module will automatically add your EmbAd script to your website pages.'),
  );

  $form['embad_status'] = array(
    '#type' => 'radios',
    '#title' => t('EmbAd Ads'),
    '#options' => array(1 => t('On'), 0 => t('Off')),
    '#default_value' => variable_get('embad_status', 1),
    '#size' => 10,
    '#required' => TRUE,
  );

  $form['embad_pid'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID'),
    '#description' => t('Please enter your account pid'),
    '#default_value' => variable_get('embad_pid', ''),
    '#required' => TRUE,
    '#size' => 10,
  );

  $form['embad_wsid'] = array(
    '#type' => 'textfield',
    '#title' => t('Website ID'),
    '#description' => t('Please enter your website wsid'),
    '#default_value' => variable_get('embad_wsid', ''),
    '#required' => TRUE,
    '#size' => 10,
  );

  $form['embad_registered_members'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable ads for registered members'),
    '#default_value' => variable_get('embad_registered_members', 0),
  );

  return system_settings_form($form);
}

/**
 * Form validation for embAD settings.
 */
function embad_admin_settings_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['embad_pid'])) {
    form_set_error('embad_pid', t('You must enter a number for "Publisher ID".'));
  }

  if (!is_numeric($form_state['values']['embad_wsid'])) {
    form_set_error('embad_wsid', t('You must enter a number for "Website ID".'));
  }
}
